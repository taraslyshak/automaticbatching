﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Shared;
using Shared.Models;

namespace Reporting.Controllers
{
    public class ReportingController : Controller
    {
        public const string AllString = "All";
        private DatabaseContext Db = DatabaseContext.Instance();

        public ActionResult Reporting()
        {
            
            var searchParameters = new List<string> {"Name","Number"};
            ViewData.Add("SearchParameters", searchParameters);

            var formulaNames = new List<string>(Db.Formulas.Select(s => s.FormulaName)) {AllString};
            ViewData.Add("FormulaNames", formulaNames);

            var formulaNumbers = new List<string>(Db.Formulas.Select(s => s.FormulaNo.ToString())) {AllString};
            ViewData.Add("FormulaNumbers", formulaNumbers);

            var materialsNames = new List<string>(Db.Materials.Select(s => s.MaterialName)) {AllString};
            ViewData.Add("MaterialsNames", materialsNames);

            var materialsNumbers = new List<string>(Db.Materials.Select(s => s.MaterialNo.ToString())) {AllString};
            ViewData.Add("MaterialsNumbers", materialsNumbers);

            ViewData.Add("BatchReportEndDate", DateTime.Now);
            ViewData.Add("BatchReportStartDate", DateTime.Now.AddYears(-1));

            ViewData.Add("FormulaReportEndDate",  DateTime.MinValue );
            ViewData.Add("FormulaReportStartDate",  DateTime.MaxValue );
            ViewData.Add("FormulaReportNumber", new object[] {7, 8, 901});
        
            ViewData.Add("MaterialReportEndDate", DateTime.Now.AddYears(-10) );
            ViewData.Add("MaterialReportStartDate",  DateTime.Now.AddYears(10) );
            ViewData.Add("MaterialReportStartFormulaNumber",  new object[] { 7, 8, 901 } );
            ViewData.Add("MaterialReportStartmaterialNumber",  new object[] { 17, 33, 37 } );

            var autoReportsInfo = Db.AutoReportsInfo.FirstOrDefault();
            if (autoReportsInfo != null)
            {
                ViewData.Add("Repository", autoReportsInfo.StoreReportsDirectory);
                ViewData.Add("MorningShiftStart",autoReportsInfo.MorningShift);
                ViewData.Add("isTwoShift",autoReportsInfo.ShiftsPerDay==2);
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session["user"] = null;
            return RedirectToAction("Reporting");
        }

       
        [HttpPost]
        public ActionResult Reporting(UserModel user)
        {
            var logineduser = Db.Users.SingleOrDefault(p => p.Name == user.Name && p.Password == user.Password);
            if (logineduser == null) return Json(false);
            Session["user"] = new LoginedUser
            {
                Isloged = true,
                Name = logineduser.Name,
                UserRoleId = logineduser.UserRoleId,
                TimeToEndSession = 900
            };
            ViewData.Add("TimeToEndSession", 900);
            return RedirectToAction("Reporting");
        }

        [HttpPost]
        public ActionResult CheckLogin(string name, string password)
        {
            var logineduser = Db.Users.SingleOrDefault(p => p.Name == name && p.Password == password);
            return Json(logineduser != null);
        }
        public ActionResult TimerTick()
        {
            var user = Session["user"] as LoginedUser;
            if (user == null) return Json("LogOut");
            var minutes = user.TimeToEndSession / 60;
            var seconds = user.TimeToEndSession % 60;
            if (minutes == 0 && seconds == 0) return Json("LogOut");
            user.TimeToEndSession--;
            Session["user"] = user;
            return Json($"{minutes}:{seconds}");
        }

        public ActionResult SaveChanges(bool shift, string repository, DateTime time)
        {
            var autoreportinfo = Db.AutoReportsInfo.FirstOrDefault();
            autoreportinfo.ShiftsPerDay = shift ? 2 : 3;
            autoreportinfo.StoreReportsDirectory = repository;
            autoreportinfo.MorningShift = time.TimeOfDay;
            Db.AutoReportsInfo.Attach(autoreportinfo);
            Db.SaveChanges();

            return Json(true);
        }

        public ActionResult ShiftPeriod(string shift)
        {
            DateTime startDate=new DateTime();
            DateTime endDate = new DateTime();
            double shiftDuration = (double) (24 / Db.AutoReportsInfo.First().ShiftsPerDay);
            var shiftStart = DateTime.Parse(Db.AutoReportsInfo.First().MorningShift.ToString());
            switch (shift)
            {
                case "This":
                   
                    int shiftNumber2 = (int)((DateTime.Now.TimeOfDay.Hours - shiftStart.TimeOfDay.Hours) / shiftDuration);
                    
                   startDate = DateTime.Today.AddHours(shiftStart.TimeOfDay.Hours).AddMinutes(
                           shiftStart.TimeOfDay.Minutes).AddHours((shiftNumber2 * shiftDuration));
                   endDate = startDate.AddHours(shiftDuration);
                    return Json(new List<DateTime> {startDate, endDate});
                    
                case "Last":
                    var timeFromStart = DateTime.Now.TimeOfDay - shiftStart.TimeOfDay;
                    if (timeFromStart.Hours < shiftDuration)
                    {
                        startDate =
                             DateTime.Today.AddDays(-1).AddHours(shiftStart.TimeOfDay.Hours).AddHours(((double)Db.AutoReportsInfo.First().ShiftsPerDay - 1) * shiftDuration).AddMinutes(
                                shiftStart.TimeOfDay.Minutes);
                        endDate = startDate.AddHours(shiftDuration);
                        return Json(new List<DateTime> { startDate, endDate });
                    }
                    int shiftNumber = (int)(timeFromStart.Hours / shiftDuration);
                    startDate = DateTime.Today.AddHours(shiftStart.TimeOfDay.Hours).AddMinutes(
                        shiftStart.TimeOfDay.Minutes).AddHours((shiftNumber - 1) * shiftDuration);
                    endDate = startDate.AddHours(shiftDuration);
                    return Json(new List<DateTime> { startDate, endDate });
            }
            return Json(true);
        }

        public ActionResult ReportParameters(DateTime startdate,DateTime starttime, DateTime enddate, DateTime endtime, string materials, string materialsearchby, string formulas,string formulasearchby, string typeofreport)
        {
            switch (typeofreport)
            {
                case "MaterialsReport":
                    var materialNamesArgs = new List<int>();
                    if (formulas == "All")
                    {
                        materialNamesArgs = Db.Materials.Select(f => f.MaterialNo).ToList();
                    }
                    else
                    {
                        materialNamesArgs.Add(materialsearchby == "Name"
                            ? Db.Materials.First(f => f.MaterialName == materials).MaterialNo
                            : int.Parse(materials));
                    }
                       
                    var formulaNamesArgs1 = new List<int>();
                    if (formulas == "All")
                    {
                        formulaNamesArgs1 = Db.Formulas.Select(f => f.FormulaNo).ToList();
                    }
                    else
                    {
                        formulaNamesArgs1.Add(formulasearchby == "Name"
                            ? Db.Formulas.First(f => f.FormulaName == formulas).FormulaNo
                            : int.Parse(formulas));
                    }

                    ViewData["MaterialReportEndDate"]=  new DateTime(enddate.Year, enddate.Month, enddate.Day, endtime.Hour, endtime.Minute, endtime.Second);
                            ViewData["MaterialReportStartDate"]= new DateTime(startdate.Year, startdate.Month, startdate.Day, starttime.Hour, starttime.Minute, starttime.Second);
                            ViewData["MaterialReportStartFormulaNumber"]= formulaNamesArgs1 ;
                        ViewData["MaterialReportStartmaterialNumber"]=  materialNamesArgs;
                    Dictionary<string, object> materialsource = new Dictionary<string, object>();
                    materialsource.Add("1", new DateTime(enddate.Year, enddate.Month, enddate.Day, endtime.Hour, endtime.Minute, endtime.Second));
                    materialsource.Add("2", new DateTime(startdate.Year, startdate.Month, startdate.Day, starttime.Hour, starttime.Minute, starttime.Second));
                    materialsource.Add("3", formulaNamesArgs1);
                    materialsource.Add("4", materialNamesArgs);
                    return Json(materialsource);
                    
                case "FormulaReport":
                    var formulaNamesArgs=new List<int>();
                    if (formulas == "All")
                    {
                        formulaNamesArgs = Db.Formulas.Select(f => f.FormulaNo).ToList();
                    }
                    else
                    {
                        formulaNamesArgs.Add(formulasearchby == "Name"
                            ? Db.Formulas.First(f => f.FormulaName == formulas).FormulaNo
                            : int.Parse(formulas));
                    }
                    


                    ViewData["FormulaReportEndDate"]=new DateTime(enddate.Year, enddate.Month, enddate.Day, endtime.Hour, endtime.Minute, endtime.Second);
                    ViewData["FormulaReportStartDate"]= new DateTime(startdate.Year, startdate.Month, startdate.Day, starttime.Hour, starttime.Minute, starttime.Second);
                    ViewData["FormulaReportNumber"]= formulaNamesArgs;
                    Dictionary<string, object> dicti = new Dictionary<string, object>();
                    dicti.Add("1", new DateTime(enddate.Year, enddate.Month, enddate.Day, endtime.Hour, endtime.Minute, endtime.Second));
                    dicti.Add("2", new DateTime(startdate.Year, startdate.Month, startdate.Day, starttime.Hour, starttime.Minute, starttime.Second));
                    dicti.Add("3", formulaNamesArgs);
                    return Json(dicti);
                    

                case "BatchReport":
                    ViewData["BatchReportEndDate"]= new DateTime(enddate.Year, enddate.Month, enddate.Day, endtime.Hour, endtime.Minute, endtime.Second);
                    ViewData["BatchReportStartDate"]= new DateTime(startdate.Year, startdate.Month, startdate.Day, starttime.Hour, starttime.Minute, starttime.Second);
                    
                    return Json(new[] { new DateTime(enddate.Year, enddate.Month, enddate.Day, endtime.Hour, endtime.Minute, endtime.Second),
                        new DateTime(startdate.Year, startdate.Month, startdate.Day, starttime.Hour, starttime.Minute, starttime.Second)});
            
            }
            
            return Json(true);
        }
    }
}