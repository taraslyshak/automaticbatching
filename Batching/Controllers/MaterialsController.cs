﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Shared;
using Shared.Models;
using Materials = Shared.Materials;

namespace KendoUIApp1.Controllers
{
    public class MaterialsController : Controller
    {
        private DatabaseContext Db = DatabaseContext.Instance();
        
        public ActionResult Material()
        {
            var searchParameters = new List<string> { "Name", "Number" };
            ViewData.Add("SearchParameters", searchParameters);
            ViewData.Add("SelectedSearchParameter",searchParameters.FirstOrDefault());
            IEnumerable<MatlTypesModel> matlTypes = Db.MatlTypes.Select(p => new MatlTypesModel
            {
                TypeID = p.TypeID,
                TypeNo = p.TypeNo,
                TypeName = p.TypeName
            });
            ViewData.Add("MatlTypes", matlTypes);
            ViewData.Add("defaultMatlType", matlTypes.FirstOrDefault());
            return View(GetMaterials());
        }

        public ActionResult Materials_ReadFirstGrid([DataSourceRequest] DataSourceRequest request, string selectedParam, string searchParam, string sortedBy, bool sortedParam)
        {
            IEnumerable<Materials> materials;
            var sort = new SortDescriptor(sortedBy, sortedParam? ListSortDirection.Ascending: ListSortDirection.Descending);
            request.Sorts = new List<SortDescriptor>(new []{sort});
            if (selectedParam == null || searchParam==null)materials = Db.Materials;
            else
            if(selectedParam =="Name")
                materials = searchParam == string.Empty
                     ? Db.Materials
                     : Db.Materials.Where(s => s.MaterialName.ToLower().Contains(searchParam.ToLower()));
            else
            materials = searchParam == string.Empty
                        ? Db.Materials
                        : Db.Materials.Where(s => s.MaterialNo.ToString().Equals(searchParam));
            switch (sortedBy)
            {
                case "MaterialNo":
                    materials = sortedParam ? materials.OrderBy(m => m.MaterialNo).Take(materials.Count() / 2) : materials.OrderByDescending(m => m.MaterialNo).Take(materials.Count() / 2);
                    break;
                case "MaterialName":
                    materials = sortedParam ? materials.OrderBy(m => m.MaterialName).Take(materials.Count() / 2) : materials.OrderByDescending(m => m.MaterialName).Take(materials.Count() / 2);
                    break;
                case "TypeID":
                    materials = sortedParam ? materials.OrderBy(m => m.TypeID).Take(materials.Count() / 2) : materials.OrderByDescending(m => m.TypeID).Take(materials.Count() / 2);
                    break;
            }
           
            var result = materials.ToDataSourceResult(request, p => new MaterialModel
                {
                    MaterialNo = p.MaterialNo,
                    MaterialName = p.MaterialName,
                    MaterialID = p.MaterialID,
                    TypeID = p.TypeID,
                    MatlTypes = new MatlTypesModel
                    {
                        TypeID = p.MatlTypes.TypeID,
                        TypeName = p.MatlTypes.TypeName,
                        TypeNo = p.MatlTypes.TypeNo
                    }
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            

            
        }
        public ActionResult Materials_ReadSecondGrid([DataSourceRequest] DataSourceRequest request, string selectedParam, string searchParam, string sortedBy, bool sortedParam)
        {
            IEnumerable<Materials> materials;
            var sort = new SortDescriptor(sortedBy, sortedParam ? ListSortDirection.Ascending : ListSortDirection.Descending);
            request.Sorts = new List<SortDescriptor>(new[] { sort });
            if (selectedParam == null || searchParam == null) materials = Db.Materials;
            else
            if (selectedParam == "Name")
                materials = searchParam == string.Empty
                     ? Db.Materials
                     : Db.Materials.Where(s => s.MaterialName.ToLower().Contains(searchParam.ToLower()));
            else
                materials = searchParam == string.Empty
                            ? Db.Materials
                            : Db.Materials.Where(s => s.MaterialNo.ToString().Equals(searchParam));

            switch (sortedBy)
            {
                case "MaterialNo":
                    materials = sortedParam ? materials.OrderBy(m => m.MaterialNo).Skip(materials.Count() / 2) : materials.OrderByDescending(m => m.MaterialNo).Skip(materials.Count() / 2);
                    break;
                case "MaterialName":
                    materials = sortedParam ? materials.OrderBy(m => m.MaterialName).Skip(materials.Count() / 2) : materials.OrderByDescending(m => m.MaterialName).Skip(materials.Count() / 2);
                    break;
                case "TypeID":
                    materials = sortedParam ? materials.OrderBy(m => m.TypeID).Skip(materials.Count() / 2) : materials.OrderByDescending(m => m.TypeID).Skip(materials.Count() / 2);
                    break;
            }
            
            var result = materials.ToDataSourceResult(request, p => new MaterialModel
            {
                MaterialNo = p.MaterialNo,
                MaterialName = p.MaterialName,
                MaterialID = p.MaterialID,
                TypeID = p.TypeID,
                MatlTypes = new MatlTypesModel
                {
                    TypeID = p.MatlTypes.TypeID,
                    TypeName = p.MatlTypes.TypeName,
                    TypeNo = p.MatlTypes.TypeNo
                }
            });
            return Json(result, JsonRequestBehavior.AllowGet);



        }


        public ActionResult UpdateCreateDelete(
            [Bind(Prefix = "updated")] List<MaterialModel> updatedmaterials,
            [Bind(Prefix = "new")] List<MaterialModel> newmaterials,
            [Bind(Prefix = "deleted")] List<MaterialModel> deletedmaterials)
        {
            if (updatedmaterials != null && ModelState.IsValid) Materials_Update(updatedmaterials );
            if (newmaterials != null && newmaterials.Count > 0) Materials_Create(newmaterials);
            if (deletedmaterials != null && deletedmaterials.Count > 0) Materials_Destroy(deletedmaterials);
            return Json("Success!");
       }

        public ActionResult Delete([Bind(Prefix = "deleted")] List<MaterialModel> deletedmaterials)
        {
           if (deletedmaterials != null && deletedmaterials.Count > 0) Materials_Destroy(deletedmaterials);
            return Json("Success!");
        }

        public ActionResult AddNewMaterial()
        {
            Db.Materials.Add(new Materials
            {
                MaterialNo = Db.Materials.Max(s => s.MaterialNo) + 1,
                MaterialName = "NewMaterial",
                MatlTypes = Db.MatlTypes.FirstOrDefault(),
                MaterialID = Db.Materials.Max(s => s.MaterialID) + 1
            });
            Db.SaveChanges();
            return Json(true);
        }

        private void Materials_Create(IEnumerable<MaterialModel> newmaterials)
        {
            foreach (var material in newmaterials)
            {
                var entity = new Materials
                {
                    MaterialID = Db.Materials.Max(p => p.MaterialID) + 1,
                    MaterialName = material.MaterialName,
                    MaterialNo = Db.Materials.Max(p => p.MaterialNo) + 1,
                    TypeID = material.TypeID,
                    MatlTypes = new MatlTypes
                    {
                        TypeID = material.MatlTypes.TypeID,
                        TypeNo = material.MatlTypes.TypeNo,
                        TypeName = material.MatlTypes.TypeName
                    }

                };
                Db.Materials.Add(entity);
                Db.SaveChanges();

            }
        }

        private void Materials_Destroy(IEnumerable<MaterialModel> deletedmaterials)
        {
            foreach ( var material in deletedmaterials.Select(material => new Materials
                          {
                              MaterialID = material.MaterialID,
                              MaterialNo = material.MaterialNo,
                              MaterialName = material.MaterialName,
                              TypeID = material.TypeID,
                              MatlTypes = new MatlTypes
                              {
                                  TypeID = material.MatlTypes.TypeID,
                                  TypeName = material.MatlTypes.TypeName,
                                  TypeNo = material.MatlTypes.TypeNo
                              }
                          }))
            {
                Db.Materials.Remove(Db.Materials.Single(m => m.MaterialID==material.MaterialID));
                var formulaSteps = Db.FormulaSteps.Where(p => p.MaterialID == material.MaterialID);
                foreach (var step in new List<FormulaSteps>(formulaSteps))
                {
                    Db.FormulaSteps.Remove(step);
                }
            }
            Db.SaveChanges();
        }

        private void Materials_Update(IEnumerable<MaterialModel> updatedmaterials)
        {
            foreach (var entity in updatedmaterials.Select(material => new Materials
            {
                MaterialID = material.MaterialID,
                MaterialName = material.MaterialName,
                MaterialNo = material.MaterialNo,
                TypeID = material.TypeID,
                
            }))
            {
                Db.Materials.Remove(Db.Materials.Single(m=>m.MaterialID==entity.MaterialID));
                Db.Materials.Add(entity);
                Db.SaveChanges();
            }
        }

        public ActionResult CheckManterialNoDuplicate([Bind(Prefix = "updated")] List<MaterialModel> updatedmaterials)
        {
            if (updatedmaterials == null) return Json(false);
            foreach (var material in updatedmaterials)
            {
                return
                    Json(
                        Db.Materials.Any(m => m.MaterialID != material.MaterialID && m.MaterialNo == material.MaterialNo));
            }
            return Json(false);
        }

        public IEnumerable<MaterialModel> GetMaterials()
        {
            return Db.Materials.Select(p => new MaterialModel
            {
                TypeID = p.TypeID,
                MaterialID = p.MaterialID,
                MaterialName = p.MaterialName,
                MaterialNo = p.MaterialNo,
                MatlTypes = new MatlTypesModel
                {
                    TypeID = p.MatlTypes.TypeID,
                    TypeName = p.MatlTypes.TypeName,
                    TypeNo = p.MatlTypes.TypeNo
                }
            }).ToList();
        }

    }
}