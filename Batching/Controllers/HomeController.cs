﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Shared;
using Shared.Models;
using FormulaSteps = Shared.FormulaSteps;


namespace KendoUIApp1.Controllers
{
    public class HomeController : Controller
    {
        #region Constants
        private const string DefaultFormulaName = "New Formula";
        private const int DefaultFormulaVersion = 1;
        private const int DefaultFormulaMixTime = 0;
        private const int DefaultFormulaSusceptibility = 0;
        private const int DefaultFormulaMolasses = 0;
        private const int DefaultFormulaTallow = 0;
        private const int DefaultFormulaConditioning = 0;
        private const int DefaultFormulaContamination = 0;

        private const int DefaultFormulaStepAmount = 0;
        private const int DefaultFormulaStepInFlight = 0;
        private const int DefaultFormulaStepNegTolerance = 0;
        private const int DefaultFormulaStepPosTolerance = 0;

        #endregion

        private DatabaseContext Db = DatabaseContext.Instance();
       
        public ActionResult Index()
        {
            var searchParameters = new List<string> { "Name", "Number" };
            ViewData.Add("SearchParameters", searchParameters);
            ViewData.Add("SelectedSearchParameter", searchParameters.FirstOrDefault());
            Session["ListOfChangedFormulas"]=new List<int>();

            Session["selectedFormula"]= GetSelectedFormula(Db.Formulas.FirstOrDefault());
            IEnumerable<MaterialModel> materials = Db.Materials.Select(p => new MaterialModel
            {
                MaterialID = p.MaterialID,
                TypeID = p.TypeID,
                MaterialName = p.MaterialName,
                MaterialNo = p.MaterialNo

            });
            ViewData.Add("materials", materials);
            ViewData.Add("Defaultmaterials",materials.FirstOrDefault());
            IEnumerable<MatlTypesModel> matlTypes = Db.MatlTypes.Select(p => new MatlTypesModel
            {
                TypeID = p.TypeID,
                TypeNo = p.TypeNo,
                TypeName = p.TypeName
            });
            ViewData.Add("MatlTypes", matlTypes);
            ViewData.Add("defaultMatlType", matlTypes.FirstOrDefault());
            return View(Db.Formulas.ToList());
        }
        
        public ActionResult FormulaClone(string id, string newName)
        {
            var selectedFormula = GetSelectedFormula(Db.Formulas.FirstOrDefault(f => f.FormulaNo.ToString() == id));
            Session["selectedFormula"] = selectedFormula;
            var selectedFormulaSteps = GetSelectedFormulaSteps(selectedFormula);
            var user = Session["user"] as LoginedUser;
            var formula = new Formulas
            {
                FormulaID = Db.Formulas.Any() ? Db.Formulas.Max(s => s.FormulaID) + 1 : 1,
                FormulaName = newName,
                Author = user.Name,
                FormulaNo = Db.Formulas.Max(s => s.FormulaNo) + 1,
                Version = 1,
                CreateDate = DateTime.Now,
                ModifyDate = DateTime.Now,
                MixTime = selectedFormula.MixTime,
                Susceptibility = selectedFormula.Susceptibility,
                Molasses = selectedFormula.Molasses,
                Tallow = selectedFormula.Tallow,
                Conditioning = selectedFormula.Conditioning,
                Contamination = selectedFormula.Contamination,
            };
            foreach (FormulaStepModel selectedFormulaStep in selectedFormulaSteps)
            {
                formula.FormulaSteps.Add(new FormulaSteps
                {
                    StepNo = selectedFormulaStep.StepNo,
                    MaterialID = selectedFormulaStep.MaterialID,
                    Amount = selectedFormulaStep.Amount,
                    InFlight = selectedFormulaStep.InFlight,
                    PosTolerance = selectedFormulaStep.PosTolerance,
                    NegTolerance = selectedFormulaStep.NegTolerance
                });
            }
            Db.Formulas.Add(formula);
            Db.SaveChanges();
            return Json(true);
        }
        
        public ActionResult FormulaDelete(string id)
        {
            Db.Formulas.Remove(Db.Formulas.FirstOrDefault(f => f.FormulaNo.ToString() == id));
            Db.SaveChanges();
            DataSourceResult data = Db.Formulas.ToDataSourceResult(new DataSourceRequest(), p => new FormulaModel
            {
                FormulaNo = p.FormulaNo,
                Author = p.Author,
                CreateDate = p.CreateDate,
                FormulaName = p.FormulaName
            });
            return Json(data);

        }

        public ActionResult Formulas_Read([DataSourceRequest] DataSourceRequest request, string selectedParam, string searchParam, string sortedBy, bool sortedParam)
        {
            IEnumerable<Formulas> formulas;
            var sort = new SortDescriptor(sortedBy, sortedParam ? ListSortDirection.Ascending : ListSortDirection.Descending);
            request.Sorts = new List<SortDescriptor>(new[] { sort });
            if (selectedParam == null || searchParam == null) formulas = Db.Formulas;
            else
           if (selectedParam == "Name")
                formulas = searchParam == string.Empty
                     ? Db.Formulas
                     : Db.Formulas.Where(s => s.FormulaName.ToLower().Contains(searchParam.ToLower()));
            else
                formulas = searchParam == string.Empty
                            ? Db.Formulas
                            : Db.Formulas.Where(s => s.FormulaNo.ToString().Equals(searchParam));

            DataSourceResult result = formulas.ToDataSourceResult(request, p => new FormulaModel()
            {
                FormulaNo = p.FormulaNo,
                Author = p.Author,
                CreateDate = p.CreateDate,
                FormulaName = p.FormulaName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Formula_Create()
        {
            var user = Session["user"] as LoginedUser;
            Db.Formulas.Add(new Formulas
            {
                FormulaID = Db.Formulas.Any() ? Db.Formulas.Max(s => s.FormulaID) + 1 : 1,
                FormulaName = DefaultFormulaName,
                FormulaNo = Db.Formulas.Any() ? Db.Formulas.Max(s => s.FormulaNo) + 1 : 1,
                Author = user.Name,
                Version = DefaultFormulaVersion,
                CreateDate = DateTime.Now,
                ModifyDate = DateTime.Now,
                MixTime = DefaultFormulaMixTime,
                Susceptibility = DefaultFormulaSusceptibility,
                Molasses = DefaultFormulaMolasses,
                Tallow = DefaultFormulaTallow,
                Conditioning = DefaultFormulaConditioning,
                Contamination = DefaultFormulaContamination
            });
            Db.SaveChanges();
            return Json(true);
        }
       
        public ActionResult FormulaSteps_Read([DataSourceRequest] DataSourceRequest request )
        {
            var test = (FormulaModel)Session["selectedFormula"];
            var formulaSteps = GetSelectedFormulaSteps(test);

            DataSourceResult result = formulaSteps.ToDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateCreateDelete(
            [Bind(Prefix = "updated")] List<FormulaStepModel> updatedFormulaSteps)
        {
            if (updatedFormulaSteps != null && ModelState.IsValid) FormulaStep_Update(updatedFormulaSteps);
            return Json("Success!");
        }

        public void FormulaStep_Update(IEnumerable<FormulaStepModel> updatedFormulaStes)
        {
            foreach (var step in updatedFormulaStes.Select(s=> new FormulaSteps
            {
                Amount = s.Amount,
                FormulaID = s.FormulaID,
                InFlight = s.InFlight,
                NegTolerance = s.NegTolerance,
                PosTolerance = s.PosTolerance,
                MaterialID = s.MaterialID,
                StepNo = s.StepNo,
                StepID = s.StepID
            }))
            {
                Db.FormulaSteps.Remove(Db.FormulaSteps.Single(u => u.StepID== step.StepID));
                Db.FormulaSteps.Add(step);
                
            }
            Db.SaveChanges();
        }

        public ActionResult AddFormulaStep()
        {
            var selectedFormula =(FormulaModel)Session["selectedFormula"];
            if (Db.Formulas.First(f=>f.FormulaID==selectedFormula.FormulaID).FormulaSteps.Count == 64) return Json(false);
            var formulaStep = new FormulaSteps
            {
                Amount = DefaultFormulaStepAmount,
                InFlight = DefaultFormulaStepInFlight,
                StepNo =!Db.Formulas.First(f => f.FormulaID == selectedFormula.FormulaID).FormulaSteps.Any() ? 1 : Db.Formulas.First(f => f.FormulaID == selectedFormula.FormulaID).FormulaSteps.Select(s => s.StepNo).Max() + 1,
                NegTolerance = DefaultFormulaStepNegTolerance,
                PosTolerance = DefaultFormulaStepPosTolerance,
                Materials = Db.Materials.FirstOrDefault(),
                FormulaID = selectedFormula.FormulaID,
                // MaterialID = DefaultFormulaStepMaterialId
            };
            Db.FormulaSteps.Add(formulaStep);
            Db.SaveChanges();
            var changedFormulaList = (List<int>)Session["ListOfChangedFormulas"];
            var id = changedFormulaList.SingleOrDefault(f => f == selectedFormula.FormulaID);
            if (id == 0) changedFormulaList.Add(selectedFormula.FormulaID);
            Session["ListOfChangedFormulas"] = changedFormulaList;

            return Json(true);
        }
        public ActionResult InsertFormulaStep(int id)
        {
            var formula = (FormulaModel)Session["selectedFormula"];
            var selectedFormula = Db.Formulas.First(f => f.FormulaID == formula.FormulaID);
            if (selectedFormula.FormulaSteps.Count == 64) return Json(false);
            var selectedStep = selectedFormula.FormulaSteps.First(s => s.StepNo == id);
            foreach (var step in selectedFormula.FormulaSteps.Where(s => s.StepNo > id))
            {
                step.StepNo++;
            }
            var formulaStep = new FormulaSteps
            {
                Amount = DefaultFormulaStepAmount,
                InFlight = DefaultFormulaStepInFlight,
                StepNo = selectedStep.StepNo + 1,
                NegTolerance = DefaultFormulaStepNegTolerance,
                PosTolerance = DefaultFormulaStepPosTolerance,
                Materials = Db.Materials.FirstOrDefault(),
                FormulaID = selectedStep.FormulaID,
                // MaterialID = DefaultFormulaStepMaterialId
            };
            Db.FormulaSteps.Add(formulaStep);
            Db.SaveChanges();
            var changedFormulaList = (List<int>)Session["ListOfChangedFormulas"];
            var formid = changedFormulaList.SingleOrDefault(f => f == selectedFormula.FormulaID);
            if (formid == 0) changedFormulaList.Add(selectedFormula.FormulaID);
            Session["ListOfChangedFormulas"] = changedFormulaList;
            return Json(true);
        }
        public ActionResult DeleteFormulaStep(int id)
        {
            var formula = (FormulaModel)Session["selectedFormula"];
            var selectedFormula = Db.Formulas.First(f => f.FormulaID == formula.FormulaID);
            var selectedStep = selectedFormula.FormulaSteps.First(s => s.StepNo == id);
            foreach (var step in selectedFormula.FormulaSteps.Where(s=>s.StepNo > id))
            {
                step.StepNo--;
            }
            Db.FormulaSteps.Remove(selectedStep);
            Db.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public ActionResult FormulaDetailsChanged(FormulaModel formula)
        {
            var formulaToChange = Db.Formulas.First(f => f.FormulaID == formula.FormulaID);
            formulaToChange.FormulaNo = formula.FormulaNo;
            formulaToChange.FormulaName = formula.FormulaName;
            formulaToChange.MixTime = formula.MixTime;
            formulaToChange.Molasses = formula.Molasses;
            formulaToChange.Tallow = formula.Tallow;
            formulaToChange.Conditioning = formula.Conditioning;
            formulaToChange.Contamination = formula.Contamination;
            formulaToChange.Susceptibility = formula.Susceptibility;
            Db.Formulas.Attach(formulaToChange);
            Db.Entry(formulaToChange).State = EntityState.Modified;
            Db.SaveChanges();
            var changedFormulaList =(List<int>)Session["ListOfChangedFormulas"];
            var id = changedFormulaList.SingleOrDefault(f=>f== formula.FormulaID);
            if (id==0) changedFormulaList.Add(formula.FormulaID);
            Session["ListOfChangedFormulas"] = changedFormulaList;
            return Json(true);
        }


        [HttpPost]
        public ActionResult SelectFormula(string id)
        {
            Session["selectedFormula"] = GetSelectedFormula(Db.Formulas.Single(f => f.FormulaNo.ToString() == id));
            return RedirectToAction("PartialDetails", (FormulaModel)Session["selectedFormula"]);
        }

        [HttpGet]
        public ActionResult PartialDetails(FormulaModel selected)
        {
            return PartialView("FormulaDetails", selected);
        }

        [HttpPost]
        public ActionResult Index(UserModel user)
        {
            var logineduser = Db.Users.SingleOrDefault(p => p.Name == user.Name && p.Password == user.Password);
            if (logineduser == null) return Json(false);
            Session["user"] = new LoginedUser
            {
                Isloged = true,
                Name = logineduser.Name,
                UserRoleId = logineduser.UserRoleId,
                TimeToEndSession = 900
            };
            ViewData.Add("TimeToEndSession", 900);
            return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public ActionResult CheckLogin(string name, string password)
        {
            var logineduser = Db.Users.SingleOrDefault(p => p.Name == name && p.Password == password);
            return Json(logineduser != null);
        }

        [HttpPost]
        public ActionResult CheckFormulaNoDuplicate(int number, int formulaid)
        {
            return Json(Db.Formulas.Any(f=>f.FormulaID != formulaid && f.FormulaNo == number));
        }

        public ActionResult Logout()
        {
            Session["user"] = null;
            return RedirectToAction("Index");
        }

        public IEnumerable<FormulaStepModel> GetSelectedFormulaSteps(FormulaModel model)
        {
            if (model == null) return null;
            var steps =
                Db.FormulaSteps.Where(s => s.FormulaID == model.FormulaID).OrderBy(s => s.StepNo);

            return steps.Select(step => new FormulaStepModel
            {

                MaterialTypes = new MatlTypesModel
                {
                    TypeID = step.Materials.MatlTypes.TypeID,
                    TypeName = step.Materials.MatlTypes.TypeName,
                    TypeNo = step.Materials.MatlTypes.TypeNo
                },
                Materials = new MaterialModel
                {
                    MaterialID = step.Materials.MaterialID,
                    MaterialName = step.Materials.MaterialName,
                    MaterialNo = step.Materials.MaterialNo,
                    
                },
                FormulaID = step.FormulaID,
                Amount = step.Amount,
                InFlight = (float) step.InFlight,
                NegTolerance = (float)step.NegTolerance,
                PosTolerance = (float)step.PosTolerance,
                StepNo = step.StepNo,
                StepID = step.StepID,
                MaterialID = step.MaterialID
            }).ToList();


        }

        public ActionResult TimerTick()
        {
            var user = Session["user"] as LoginedUser;
            if (user == null) return Json("LogOut");
            var minutes = user.TimeToEndSession/60;
            var seconds = user.TimeToEndSession%60;
            if (minutes == 0 && seconds == 0) return Json("LogOut");
            user.TimeToEndSession--;
            Session["user"] = user;
            return Json($"{minutes}:{seconds}");
        }

        public ActionResult SaveChanges()
        {
            var changedFormulaList = (List<int>)Session["ListOfChangedFormulas"];
            foreach (var formula in changedFormulaList.Select(item => Db.Formulas.First(f => f.FormulaID == item)))
            {
                formula.Version++;
                formula.ModifyDate = DateTime.Now;
                Db.Formulas.Attach(formula);
                Db.Entry(formula).State = EntityState.Modified;
                Db.SaveChanges();
            }
            Session["ListOfChangedFormulas"]=new List<int>();
            return Json(true);
        }

        public FormulaModel GetSelectedFormula(Formulas formula)
        {
            return new FormulaModel
            {
                FormulaID = formula.FormulaID,
                Author = formula.Author,
                FormulaNo = formula.FormulaNo,
                FormulaName = formula.FormulaName,
                CreateDate = formula.CreateDate,
                ModifyDate = formula.ModifyDate,
                Version = formula.Version,
                MixTime = formula.MixTime,
                Molasses = formula.Molasses,
                Tallow = formula.Tallow,
                Conditioning = formula.Conditioning,
                Contamination = formula.Contamination,
                Susceptibility = formula.Susceptibility
            };
        }
    }
}