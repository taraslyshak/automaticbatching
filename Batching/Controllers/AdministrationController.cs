﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Shared;
using Shared.Models;
using UserRoles = Shared.Models.UserRoles;

namespace KendoUIApp1.Controllers
{
    public class AdministrationController : Controller
    {
        private DatabaseContext Db = DatabaseContext.Instance();

        public ActionResult Administration()
        {
            
            IEnumerable<UserRoles> roles = Db.UserRoles.Select(p => new UserRoles()
            {
                Name = p.Name,
                Id = p.Id
            });
            ViewData.Add("userRoles", roles);
            
            return View(GetUsers());
        }

        public ActionResult AddNewNewUser()
        {
            var newuser = new Users
            {
                UserID = Db.Users.Select(s => s.UserID).Max() + 1,
                Name = "Name",
                Password = "111",
                Email = "enter@newUser.email",
                UserRoleId = 1
            };
            Db.Users.Add(newuser);
            return Json(true);
        }

        public ActionResult User_Read([DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<Users> users = Db.Users;
            var result = users.ToDataSourceResult(request, p => new UserModel
            {
                Name = p.Name,
                Password = p.Password,
                UserID = p.UserID,
                UserRoleId = p.UserRoleId,
                Email = p.Email,
                UserRoles = new UserRoles
                {
                    Name = p.UserRoles.Name,
                    Id = p.UserRoles.Id
                }
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult UpdateCreateDelete(
            [Bind(Prefix = "updated")] List<UserModel> updatedusers,
            [Bind(Prefix = "new")] List<UserModel> newusers,
            [Bind(Prefix = "deleted")] List<UserModel> deletedusers)
        {
            if (updatedusers != null && ModelState.IsValid) User_Update(updatedusers);
            if (newusers != null && newusers.Count > 0) User_Create(newusers);
            if (deletedusers != null && deletedusers.Count > 0) User_Destroy(deletedusers);
            return Json("Success!");
        }

        
        public void User_Create(List<UserModel> users)
        {
            foreach (var newuser in users.Select(user => new Users
            {
                UserID = Db.Users.Select(u => u.UserID).Max() + 1,
                Name = user.Name,
                Password = user.Password,
                Email = "enter@newUser.email",
                UserRoleId = user.UserRoleId
            }))
            {
                Db.Users.Add(newuser);
            }
            Db.SaveChanges();
            
        }

        public void User_Destroy(List<UserModel> users)
        {
            foreach (var user in users.Select(user=>new Users
            {
                UserID = user.UserID,
                Name = user.Name,
                Password = user.Password,
                UserRoleId = user.UserRoleId,
                Email = user.Email
            }))
            {
                Db.Users.Attach(user);
                Db.Users.Remove(user);
            }
            Db.SaveChanges();
        }

        public void User_Update(List<UserModel> users)
        {
            foreach (var user in users)
            {
                var newuser = new Users
                {
                    UserID = user.UserID,
                    Name = user.Name,
                    Password = user.Password,
                    UserRoleId = user.UserRoleId,
                    Email = user.Email
                };
                Db.Users.Remove(Db.Users.Single(u => u.UserID == user.UserID));
                Db.Users.Add(newuser);
            }
            Db.SaveChanges();
        }

        public IEnumerable<UserModel> GetUsers()
        {
            return Db.Users.Select(user => new UserModel
            {
                UserID = user.UserID,
                Name = user.Name,
                Password = user.Password,
                Email = user.Email,
                UserRoleId = user.UserRoleId
            }).ToList();
        }
    }
}