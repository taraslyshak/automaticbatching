﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shared
{
   public class DatabaseContext:BatchingNewEntities1
    {
       protected DatabaseContext()
       {
       }

        private static DatabaseContext _instance;
        public static DatabaseContext Instance()
        {
            return _instance ?? (_instance = new DatabaseContext());
        }
    }
}
