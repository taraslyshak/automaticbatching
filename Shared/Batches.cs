//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Shared
{
    using System;
    using System.Collections.Generic;
    
    public partial class Batches
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Batches()
        {
            this.BatchSteps = new HashSet<BatchSteps>();
        }
    
        public int BatchID { get; set; }
        public System.DateTime BatchDateTime { get; set; }
        public int SetID { get; set; }
        public int BatchNumber { get; set; }
        public float BatchPercent { get; set; }
        public float BatchWeight { get; set; }
        public int Status { get; set; }
    
        public virtual BatchSets BatchSets { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BatchSteps> BatchSteps { get; set; }
    }
}
