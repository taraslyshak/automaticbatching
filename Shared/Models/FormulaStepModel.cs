﻿using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public class FormulaStepModel
    {
        [Required]
        public int StepID { get; set; }

        [Required]
        public int FormulaID { get; set; }

        [Required]
        public int StepNo { get; set; }

        [Required]
        [Range(1, 9999)]
        public int MaterialID { get; set; }

        [Required]
        [Range((0D), (9999.999D))]
        public float Amount { get; set; }
        [Required]
        public float InFlight { get; set; }


        [Required]
        [Range((-9999.999D), (9999.999D))]
        public float PosTolerance { get; set; }
        [Required]
        [Range((-9999.999D), (9999.999D))]
        public float NegTolerance { get; set; }

        
        [UIHint("MaterialEditor")]
       public virtual MaterialModel Materials { get; set; }

        [UIHint("MatlTypeEditor")]
        public virtual MatlTypesModel MaterialTypes { get; set; }
           }
}