﻿using System;

namespace Shared.Models
{
    public class LoginedUser
    {
        public string Name { get; set; }

        public UserRoles UserRoles { get; set; }

        public bool Isloged { get; set; }

        public int TimeToEndSession { get; set; }

        public int UserRoleId { get; set; }
    }
}