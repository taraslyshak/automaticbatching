﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public class FormulasViewModel
    {
        [UIHint("FormulaGridEditor")]
        public IEnumerable<FormulasViewModel> Formulas { get; set; }
    }
}