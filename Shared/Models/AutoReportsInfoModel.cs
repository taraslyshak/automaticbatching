﻿using System;

namespace Shared.Models
{
    public class AutoReportsInfoModel
    {
        public Guid Id { get; set; }

        public TimeSpan MorningShift { get; set; }

        public decimal ShiftsPerDay { get; set; }

        public string StoreReportsDirectory { get; set; }

    }
}
