﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml;

namespace Shared.Models
{
    public class MaterialModel: IValidatableObject
    {
        [Required]
        public int MaterialID { get; set; }

        [Required]
        [StringLength(40)]
        [DisplayName(@"Material Name")]
        public string MaterialName { get; set; }

        [Required]
        public int TypeID { get; set; }

        [Required]
        [Range(1,9999)]
        [DisplayName(@"Material No")]
        public int MaterialNo { get; set; }

        [UIHint("MatlTypeEditor")]
        public virtual MatlTypesModel MatlTypes { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
           if(MaterialNo==1)
                yield return new ValidationResult("Description must be supplied.");
        }
    }
}