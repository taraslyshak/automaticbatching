﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public class FormulaModel
    {
       
        public FormulaModel()
        {
            this.FormulaSteps = new HashSet<FormulaStepModel>();
        }
        [Required]
        public int FormulaID { get; set; }
        [Required]
        [Range(1, 99999)]
        public int FormulaNo { get; set; }
        [Required]
        [StringLength(50)]
        public string FormulaName { get; set; }
        [Required]
        public int Version { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime CreateDate { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime ModifyDate { get; set; }
        [Required]
        [StringLength(50)]
        public string Author { get; set; }
        [Required]
        [Range(0, 99)]
        public Nullable<int> Contamination { get; set; }
        [Required]
        [Range(0, 99)]
        public Nullable<int> Susceptibility { get; set; }
        [Required]
        [Range(0, 20)]
        public Nullable<float> Molasses { get; set; }
        [Required]
        [Range(0, 20)]
        public Nullable<float> Conditioning { get; set; }
        [Required]
        [Range(0, 20)]
        public Nullable<float> Tallow { get; set; }
        [Required]
        [Range(0, 999)]
        public Nullable<int> MixTime { get; set; }

        [Required]
        public virtual ICollection<FormulaStepModel> FormulaSteps { get; set; }
    }
}