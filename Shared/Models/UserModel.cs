﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public class UserModel
    {
        public int UserID { get; set; }

        [Required]
        [DisplayName(@"Name")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [DisplayName(@"Password")]
        [DataType(DataType.Password)]
        [StringLength(50)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        public int UserRoleId { get; set; }

        [Required]
        
        public virtual UserRoles UserRoles { get; set; }
        
    }
}