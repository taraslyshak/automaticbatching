﻿using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public class MatlTypesModel
    {

        [Required]
        public int TypeID { get; set; }

        [Required]
        [StringLength(50)]
        public string TypeName { get; set; }

        [Required]
        public int TypeNo { get; set; }
        
    }
}