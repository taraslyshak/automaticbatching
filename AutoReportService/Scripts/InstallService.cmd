:: Set directory Path of an executing Batch file as current
cd /d %~dp0

:: Install service
%WinDir%\Microsoft.NET\Framework\v4.0.30319\installutil ..\AutoReportService.exe

:: Start service
net start AutoReportService