﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Parameter = Telerik.Reporting.Parameter;

namespace AutoReportService
{
    public partial class AutoReportService : ServiceBase, INotifyPropertyChanged
    {
        #region Fields

        private readonly string _connectionString =

             ConfigurationManager.ConnectionStrings["Batching"].ConnectionString;
             
        private static Timer _timer;
        private static Timer _saveReportTimer;
        private static double _interval;

        private TimeSpan _morningShift;
        private decimal _shiftsPerDay;
        private string _storeReportsDirectory;

        #endregion

        #region Properties

        public double Interval
        {
            get { return _interval; }
            set
            {
                _interval = value;
                OnPropertyChanged("Interval");
            }
        }

        public TimeSpan MorningShift
        {
            get { return _morningShift; }
            set
            {
                _morningShift = value;
                OnPropertyChanged("MorningShift");
            }
        }

        public decimal ShiftsPerDay
        {
            get { return _shiftsPerDay; }
            set
            {
                _shiftsPerDay = value;
                OnPropertyChanged("ShiftsPerDay");
            }
        }

        public string StoreReportsDirectory
        {
            get { return _storeReportsDirectory; }
            set
            {
                _storeReportsDirectory = value;
                OnPropertyChanged("StoreReportsDirectory");
            }
        }

        #endregion

        #region Constructors

        public AutoReportService()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void FindTimerInterval()
        {
            const int totalMillisecondsPerDay = 86400000;
            double startTime = MorningShift.TotalMilliseconds;
            var timeNow = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            Interval = (totalMillisecondsPerDay - MorningShift.TotalMilliseconds) / (double)ShiftsPerDay;

            while (timeNow.TotalMilliseconds > startTime)
            {
                startTime += Interval;
            }

            _timer.Interval = startTime - timeNow.TotalMilliseconds;
            _timer.Start();
        }

        public void OnDebug()
        {
            OnStart(null);
        }


        protected override void OnStart(string[] args)
        {
            //Debugger.Launch();
            _saveReportTimer = new Timer();
            _saveReportTimer.Elapsed += SaveReport;

            _timer = new Timer();
            _timer.Elapsed += TimeElapsed;

            SqlDependency.Start(_connectionString);

            RegisterForChanges();
        }

        public void RegisterForChanges()
        {
            // Connecting to the database using our subscriber connection string and
            // waiting for changes...
            var oConnection = new SqlConnection(_connectionString);
            oConnection.Open();
            try
            {
                var oCommand = new SqlCommand(
                  "SELECT ShiftsPerDay, StoreReportsDirectory, MorningShift FROM dbo.AutoReportsInfo",
                  oConnection);
                var oDependency = new SqlDependency(oCommand);
                oDependency.OnChange += OnNotificationChange;
                var sqlDataReader = oCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    MorningShift = (TimeSpan)sqlDataReader["MorningShift"];
                    StoreReportsDirectory = (string)sqlDataReader["StoreReportsDirectory"];
                    ShiftsPerDay = (decimal)sqlDataReader["ShiftsPerDay"];

                    FindTimerInterval();
                }
            }
            catch (Exception)
            {
                Debugger.Launch();
            }
            finally
            {
                oConnection.Close();
            }
        }

        private void SaveMaterialsReport()
        {
            var reportProcessor1 = new ReportProcessor();
            var reportSource =new UriReportSource();
            reportSource.Uri = "C:\\Users\\Public\\Reports\\MaterialsReport.trdx";
            reportSource.Parameters.Add(new Parameter("StartDate",DateTime.Now.AddMilliseconds((-1) * _interval)));
            reportSource.Parameters.Add( new Parameter("EndDate", DateTime.Now));
            reportSource.Parameters.Add(new Parameter("MaterialNumbers", new object[] { 17, 20, 29 }));
            reportSource.Parameters.Add(new Parameter("FormulaNumbers", new object[] { 11, 901, 902 }));
            var result1 = reportProcessor1.RenderReport("PDF", reportSource, null);
            var fs = new FileStream(StoreReportsDirectory + @"\MaterialsReport" + Guid.NewGuid().ToString() + ".pdf",
                                       FileMode.Create);
            fs.Write(result1.DocumentBytes, 0, result1.DocumentBytes.Length);
            fs.Flush();
            fs.Close();
            
        }

        private void SaveFormulasReport()
        {
            var reportProcessor1 = new ReportProcessor();
            var reportSource = new UriReportSource();
            reportSource.Uri = "C:\\Users\\Public\\Reports\\Formulas.trdx";
            reportSource.Parameters.Add(new Parameter("StartDate", DateTime.Now.AddMilliseconds((-1) * _interval)));
            reportSource.Parameters.Add(new Parameter("EndDate", DateTime.Now));
            reportSource.Parameters.Add(new Parameter("FormulaNumbers", new object[] { 11, 901, 902 }));
            var result1 = reportProcessor1.RenderReport("PDF", reportSource, null);
            var fs = new FileStream(StoreReportsDirectory + @"\Formulas" + Guid.NewGuid().ToString() + ".pdf",
                                       FileMode.Create);
            fs.Write(result1.DocumentBytes, 0, result1.DocumentBytes.Length);
            fs.Flush();
            fs.Close();
        }

        protected override void OnStop()
        {
        }

        #endregion

        #region Event Handlers

        private void SaveReport(object sender, ElapsedEventArgs e)
        {
            
            try
            {
                SaveFormulasReport();
                SaveMaterialsReport();
                
            }
            catch (Exception)
            {
                Debugger.Launch();
            }
        }

        private void TimeElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            _saveReportTimer.Interval = Interval;
            _saveReportTimer.Start();
        }

        public void OnNotificationChange(object caller, SqlNotificationEventArgs e)
        {
            _saveReportTimer.Stop();
            RegisterForChanges();
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }
}